//const carData=require("./cars.js");
//console.log(carData);

function findCarById(carData,carId){
    if(!carData || !carId || carData.length===0){
        return [];
    }else{
        for(let i=0;i<carData.length;i++){
            if(carData[i]['id']===carId){
                return carData[i];
            }
        }
        return [];
    }
}

module.exports = findCarById;