function carYears(inventory){
    let cyear=[];
    if(!inventory || inventory.length===0 || !Array.isArray(inventory))
        return [];
        
    for(let i=0;i<inventory.length;i++){
        cyear.push(inventory[i].car_year);
    }
    return cyear;
}

module.exports = carYears;
